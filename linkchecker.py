import requests, sys
import fire
from bs4 import BeautifulSoup, SoupStrainer
from urllib.parse import urlparse, urlunparse, urljoin

class URLChecker(object):
	def check_url(self, url = 'https://www.google.com'):
		print('LINKCHECKER checking [' + url + ']...')
		all_ok = True
		site = requests.get(url)
		siteparts = urlparse(url)
		for link in BeautifulSoup(site.content, features='html.parser', parse_only=SoupStrainer('a')):
			if 'href' not in link.attrs:
				continue
			if link.attrs['href'][0:6].lower() == 'mailto':
				continue
			absolute_link = urljoin(url, link.attrs['href'])
			output = 'CHECKING: ' + absolute_link + ' ... '
			related = requests.get(absolute_link)
			output += str(related.status_code)
			print(output)
			if related.status_code >= 400:
				all_ok = False
				print('LINK NOT RESOLVING: ' + absolute_link + ' RESPONSE CODE: ' + str(related.status_code))
		if all_ok:
			print('All links OK!')

if __name__ == '__main__':
	fire.Fire(URLChecker)