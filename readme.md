# LinkChecker
A WIP; a CLI tool that checks all links on a provided page to see if they don't return 4XX or 5XX.

## Usage
This file prints out the status of links found on the page passed to it via the command line argument. If something is wrong, it’ll let you know along with the status code (anything >= 400, for now)

`python linkchecker.py check-url {url} or python linkchecker.py check-url --url={url}`

example: `python linkchecker.py check-url https://cloud.ucdavis.edu/resources`

Output:

    LINKCHECKER checking [https://cloud.ucdavis.edu/resources]...
    CHECKING: https://cloud.ucdavis.edu/resources#main-content ... 200
    CHECKING: https://cloud.ucdavis.edu/resources#primary-nav ... 200
    CHECKING: https://cloud.ucdavis.edu/ ... 200
    CHECKING: https://www.ucdavis.edu/ ... 200
    CHECKING: https://cloud.ucdavis.edu/ ... 200
    CHECKING: https://cloud.ucdavis.edu/services-available ... 200
    CHECKING: https://cloud.ucdavis.edu/data-types-list ... 200
    CHECKING: https://cloud.ucdavis.edu/case-studies ... 200
    CHECKING: http://security.ucdavis.edu/ ... 200
    CHECKING: https://cloud.ucdavis.edu/contact-us ... 200
    CHECKING: https://cloud.ucdavis.edu/resources ... 200
    CHECKING: https://cloud.ucdavis.edu/iet-cloud-services-brokerage ... 200
    CHECKING: https://cloud.ucdavis.edu/ ... 200
    CHECKING: https://cloud.ucdavis.edu/form/contact ... 200
    CHECKING: https://cloud.ucdavis.edu/iet-cloud-services-brokerage ... 200
    CHECKING: https://iet.ucdavis.edu/security ... 200
    CHECKING: https://ucdavis.slack.com/messages/C052GUB0K ... 200
    CHECKING: https://itcatalog.ucdavis.edu ... 200
    CHECKING: https://itcatalog.ucdavis.edu/service/virtualization-service ... 200
    CHECKING: https://discourse.jupyter.org/ ... 200
    CHECKING: http://datascience.ucdavis.edu/ ... 200
    CHECKING: https://www.hpc.ucdavis.edu/ ... 200
    CHECKING: https://aws.amazon.com/blogs/big-data/ ... 200
    CHECKING: https://aws.amazon.com/hpc/ ... 200
    CHECKING: https://bitbucket.org/eisenterpriseplatforms/aws-cloudformation ... 200
    CHECKING: https://docs.aws.amazon.com/cli/latest/index.html ... 200
    CHECKING: https://aws.amazon.com/quickstart/ ... 200
    CHECKING: https://calculator.s3.amazonaws.com/index.html ... 200
    CHECKING: https://aws.amazon.com/free/ ... 200
    CHECKING: https://aws.amazon.com/research-credits/ ... 200
    CHECKING: https://registry.opendata.aws/ ... 200
    CHECKING: https://aws.amazon.com/blogs/big-data/ ... 200
    CHECKING: https://aws.amazon.com/hpc/ ... 200
    CHECKING: https://bitbucket.org/eisenterpriseplatforms/aws-cloudformation ... 200
    CHECKING: https://cloud.ucdavis.edu/iet-cloud-services-brokerage ... 200
    CHECKING: https://www.ucdavis.edu/ ... 200
    CHECKING: http://ucdavis.edu/help/privacy-accessibility.html ... 200
    CHECKING: https://www.ucdmc.ucdavis.edu/itsecurity/ ... 200
    CHECKING: https://sitefarm.ucdavis.edu ... 200
    All links OK!
